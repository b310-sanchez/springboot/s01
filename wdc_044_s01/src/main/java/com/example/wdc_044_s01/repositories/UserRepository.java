package com.example.wdc_044_s01.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.wdc_044_s01.models.User;

// custom method for finding a user by their username
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
// This will be used in creating JWT (JwtToken class)
