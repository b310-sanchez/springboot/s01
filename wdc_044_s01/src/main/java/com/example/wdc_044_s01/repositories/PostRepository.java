package com.example.wdc_044_s01.repositories;

import com.example.wdc_044_s01.models.Post;
import com.example.wdc_044_s01.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
// By annotating it with @Repository, it becomes a Spring-managed repository component that handles data access operations for the "Post" entity.
public interface PostRepository extends CrudRepository<Post, Long> {
    Iterable<Post> findByUser_Username(String username);

    Iterable<Post> findByUser(User user);
}
