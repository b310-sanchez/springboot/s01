package com.example.wdc_044_s01.exceptions;

/* The UserException class is a custom exception used to handle user-related errors in the application. It allows for better error handling and provides specific error messages for user-related exceptional situations. */

public class UserException extends Exception {
    public UserException(String message){
        super(message);
    }
}
