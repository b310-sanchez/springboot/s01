package com.example.wdc_044_s01.services;

import com.example.wdc_044_s01.models.User;

import java.util.Optional;

public interface UserService {
    void createUser(User user);

    Optional<User> findByUsername(String username);
}

