package com.example.wdc_044_s01.services;

import com.example.wdc_044_s01.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    void createPost(String stringToken, Post post);
    // Now that we are generating JWT, ownership of a blog spot will be retrieved from the JWT's payload
    Iterable<Post> getPosts();

    ResponseEntity updatePost(Long id, String stringToken, Post post);
    ResponseEntity deletePost(Long id, String stringToken);
    Iterable<Post> getUserPosts(String stringToken);
}
